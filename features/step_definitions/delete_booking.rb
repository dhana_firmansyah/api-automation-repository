require 'rubygems'
require 'json'
require 'test/unit/assertions'
require 'faraday'
require 'net/http'

When(/^User send request to delete booking id$/) do
  response = Faraday.delete(
      "https://restful-booker.herokuapp.com/booking/12",
      {
          "Content-Type" => "application/json",
          "Cookie"=> "token="
      }
  )
  p response.status
  p response.body

  assert_equal '{Created}',response.body
  assert_equal '{201}',response.body

end